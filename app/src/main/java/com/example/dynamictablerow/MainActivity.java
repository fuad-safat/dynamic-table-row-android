package com.example.dynamictablerow;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    public void init(){
        TableLayout ll = (TableLayout) findViewById(R.id.Table_LayOutID);
        String[] myStringArray = {"Mamun","Jarin","Muhu","Prity","Rifat","Mahin","Safat","Sammo"};
        for (int i = 0; i < 8; i++) {

            TableRow row= new TableRow(this);
            TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
            row.setLayoutParams(lp);
            CheckBox checkBox = new CheckBox(this);
            TextView tv = new TextView(this);
            ImageButton addBtn = new ImageButton(this);
           // addBtn.setImageResource(R.drawable.add);
            ImageButton minusBtn = new ImageButton(this);
            //minusBtn.setImageResource(R.drawable.minus);
            TextView qty = new TextView(this);
            checkBox.setText(myStringArray[i]);
            qty.setText(i+"10");
            row.addView(checkBox);
            row.addView(minusBtn);
            row.addView(qty);
            row.addView(addBtn);
            ll.addView(row,i);
        }
    }



}


